var path = require('path');
var express = require('express');
var app = express();

var clientDir = path.join(__dirname, '..', 'client');

// Expose the application.
app.use(express.static(clientDir));
app.use('/bower_components', express.static(path.join(__dirname, '..', 'bower_components')));

// API
app.use('/api', require('./api'));

//app.use(function (req, res) {
//    res.sendFile(path.join(clientDir, 'index.html'));
//});


app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var APP_PORT = 3000;

console.log('server starter at '+'localhost:'+ APP_PORT);
app.listen(process.env.PORT || 3000);